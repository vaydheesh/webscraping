from pprint import pprint

import aiohttp
from unsync import unsync

from Scripts.utils import (
    script_data,
    timer,
    url_extractor,
    urls,
)


async def get_html(url) -> str:
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            resp.raise_for_status()
            html = await resp.text()
            return html


@unsync
async def parse_url(url: str):
    html = await get_html(url)
    data = script_data(html)
    return url_extractor(data)


def main():
    callable_urls = [parse_url(url) for url in urls]

    res = [callable_url.result() for callable_url in callable_urls]

    pprint(res)


if __name__ == "__main__":
    print(timer(main))
