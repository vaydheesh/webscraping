import datetime
from pprint import pprint
from typing import List

import aiohttp
from bs4 import BeautifulSoup
from unsync import unsync


def scrape_linktree(links: List[str] = None):
    if not links:
        links = [
            'https://linktr.ee/thehalloweencollector',
            'http://linktr.ee/architecture_hunter',
            'https://linktr.ee/Ig_italia',
            'https://linktr.ee/luxxumoderndesignliving',
            'https://linktr.ee/skyqueen',
            'https://linktr.ee/jpconsultoriaagora',
            'https://linktr.ee/dionisiadevona',
            'https://linktr.ee/cassandranicart',
            'https://linktr.ee/tarekelmoussa',
            'https://linktr.ee/julie.ferrez',
        ]
    start = datetime.datetime.now()

    calls = [get_page(link) for link in links]
    result = [call.result() for call in calls]
    pprint(result)
    dt = datetime.datetime.now() - start
    print(f"Asynchronous version called {len(links)} links")
    print(f"Done in {dt.total_seconds():,.2f} seconds.")


@unsync
async def get_page(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            resp.raise_for_status()
            html = await resp.text()

        soup1 = BeautifulSoup(html, 'lxml')

        result = []
        for handle in soup1.find_all('div', {'data-testid': 'StyledContainer'}):
            result.append(handle.find('a')['href'])

        return result


if __name__ == '__main__':
    scrape_linktree()
